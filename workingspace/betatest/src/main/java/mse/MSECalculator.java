package mse;

import algorithm.IAlgorithm;
import com.google.common.collect.Multimap;
import game.IGame;

/** Calculator for the MSE */
public class MSECalculator {

  final IGame game;
  final IAlgorithm algorithm;

  /**
   * Constructor for the MSE
   *
   * @param game the game which has the true shapley values
   * @param algorithm the algorithm which has the approximated shapley values
   */
  public MSECalculator(IGame game, IAlgorithm algorithm) {
    this.game = game;
    this.algorithm = algorithm;
  }

  /**
   * Calculates the MSE of the already given shapley values
   *
   * @return the MSE as a double
   */
  public double calculateMSE() {
    double mse;
    double mseSquared = 0;
    for (int player : this.game.getPlayers()) {
      mse = this.game.getShapleyValue(player) - this.algorithm.getApproximatedShapleyValue(player);
      mseSquared += mse * mse;
    }
    return mseSquared / this.game.getPlayers().size();
  }

  /**
   *
   * @param currentShapleyValue
   * @return
   */
  public double calculateMSEAtCurrentPosition(int currentPlayer, double currentShapleyValue){
    double mse;
    double mseSquared = 0;
    mse = this.game.getShapleyValue(currentPlayer) - currentShapleyValue;
    mseSquared += mse * mse;

    return mseSquared;
  }
}
