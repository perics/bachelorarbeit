package algorithm;

/** Exception when something went wrong in the Approximation algorithm. */
public class AlgorithmException extends Exception {

  /**
   * @param message The specific message why somehting went wrong.
   */
  public AlgorithmException(String message) {
    super(message);
  }
}
