package algorithm.permutationsampling;


/** Class that represents a Delta of a player for the polynomial method. */
public class DeltaForPlayer {

  private final int player;
  private double delta = 0.0;
  private int counter = 0;

  /**
   * Creates a new Delta for a player
   *
   * @param player the player which should get deltaa
   */
  public DeltaForPlayer(int player) {
    this.player = player;
  }

  /** Increases the counter */
  public void increaseCounter() {
    this.counter++;
  }

  /**
   * Adds a new delta for the player
   *
   * @param delta the delta to be added
   */
  public void addDelta(Double delta) {
    this.delta += delta;
  }

  /**
   * Getter for the player number
   *
   * @return the player
   */
  public int getPlayer() {
    return this.player;
  }

  /**
   * Calculates the approximated Shapley value of the delta.
   *
   * @return the approximated shapley value of the given player
   */
  public double calculateShapley() {
    return this.delta / this.counter;
  }
}
