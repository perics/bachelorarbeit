package algorithm.permutationsampling;

import algorithm.AlgorithmException;
import algorithm.IAlgorithm;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import game.GameException;
import game.IGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An Approximation of the shapley value based on the
 * algorithm{https://www.sciencedirect.com/science/article/abs/pii/S0305054808000804}
 */
public class PermutationSamplingAlgorithm implements IAlgorithm {

  List<DeltaForPlayer> deltaForPlayerList = new ArrayList<>();
  List<Integer> playerOrder = new ArrayList<>();
  int tSteps = 0;



  ListMultimap<Integer, Double> arrayListMultimap = ArrayListMultimap.create();

  /**
   * The constructor of the algorithm. Constructs a new instance.
   *
   * @param game the game on which the algorithm should be run through.
   */
  public PermutationSamplingAlgorithm(IGame game) {

    // Initializes the deltaforPlayer List so you have a list of deltas with every player
    for (int player : game.getPlayers()) {
      this.deltaForPlayerList.add(new DeltaForPlayer(player));
      this.playerOrder.add(player);
    }
  }

  @Override
  public void calculateApproximation(IGame game, int tSteps) throws AlgorithmException {
    this.tSteps = tSteps;
    int counter = 0;

    List<Integer> listToCheck = game.getPlayers();
    // The "bigger" one
    List<Integer> permutation = new ArrayList<>();
    double firstV;
    double secondV = 0.0;

    int moduloCounter = 1;
    double moduloNumber = tSteps/20;

    for (int i = 1; i <= this.tSteps; i++) {
      // Reset the counter after the list has been looped through
      if (counter == game.getPlayers().size() || counter == 0) {
        Collections.shuffle(listToCheck);
        counter = 0;

        permutation.clear();
        secondV = 0.0;
      }
      permutation.add(listToCheck.get(counter));

      double delta;
      int indexOfCurrentPlayer = this.playerOrder.indexOf(listToCheck.get(counter));

      try {
        firstV = game.getV(permutation);
      } catch (GameException e) {
        throw new AlgorithmException(e.getMessage());
      }
      delta = firstV - secondV;

      secondV = firstV;

      // adds the delta to the current player
      this.deltaForPlayerList.get(indexOfCurrentPlayer).addDelta(delta);
      this.deltaForPlayerList.get(indexOfCurrentPlayer).increaseCounter();

      counter++;



      if(moduloCounter % moduloNumber == 0){
        getEveryPlayerCurrentValue(listToCheck);
      }
      moduloCounter ++;

    }
  }

  /**
   *
   * @param listToCheck the list with all the players
   */
  private void getEveryPlayerCurrentValue(List<Integer> listToCheck){
    for (int player : listToCheck){
      this.arrayListMultimap.put(player, getApproximatedShapleyValue(player));
    }
  }

  /**
   * Getter for the map of players
   * @return the map with every player and their corresponding approximated shapley values
   */
  public ListMultimap<Integer, Double> getArrayListMultimap() {
    return this.arrayListMultimap;
  }


  @Override
  public double getApproximatedShapleyValue(int player) {
    return deltaForPlayerList.get(this.playerOrder.indexOf(player)).calculateShapley();
  }

  /**
   * Getter for the list of players and their delta.
   *
   * @return the list of all the players and their delta respectively.
   */
  public List<DeltaForPlayer> getDeltaForPlayerList() {
    return this.deltaForPlayerList;
  }
}
