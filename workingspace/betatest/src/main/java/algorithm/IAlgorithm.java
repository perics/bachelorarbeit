package algorithm;

import com.google.common.collect.ListMultimap;
import game.IGame;

/**
 * Algorithm class, that holds the approximation algorithm for each approximation method for the
 * shapley value.
 */
public interface IAlgorithm {

  /**
   * Approximates the shapley value of a specific game.
   *
   * @param game the game with the corresponding players, the more players the worse the
   *     approximation should be.
   * @param tSteps The amount of steps an approximation gets, the higher it is the better the result
   *     should be.
   * @throws AlgorithmException when something went wrong in the calculation.
   */
  void calculateApproximation(IGame game, int tSteps) throws AlgorithmException;

  /**
   * Returns the calculated approximated shapley value of a given player
   *
   * @param player the player whose shapley value is to be returned
   * @return the shapley value of the given player
   */
  double getApproximatedShapleyValue(int player);

  /**
   * Returns a Multimap of all the shapley values at the corresponding positions
   *
   * @return multimap with shapley values of players at corresponding positions
   */
  ListMultimap<Integer, Double> getArrayListMultimap();
}
