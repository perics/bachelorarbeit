package game;

import java.util.List;

/** A synthetic game for the shapley v */
public interface IGame {

  /**
   * Getter for the list of all players
   *
   * @return a list of all players
   */
  List<Integer> getPlayers();

  /**
   * Returns the shapley value of the corresponding player
   *
   * @param player the player to get the shapley value from
   * @return the shapley value of the specified player
   */
  double getShapleyValue(int player);

  /**
   * @param players
   * @return
   * @throws GameException
   */
  double getV(List<Integer> players) throws GameException;
}
