package game.gloves;

import game.GameException;
import game.IGame;

import java.util.ArrayList;
import java.util.List;

/**
 * The GlovesGame class implements the IGame interface, which represents a game in which each player
 * has a glove, and the goal is to match as many left gloves with their corresponding right gloves
 * as possible. Each player corresponds to a glove, and the value of the game is determined by the
 * number of left and right gloves that can be matched.
 */
public class GlovesGame implements IGame {
  /** The list of players in the game, where each player corresponds to a glove. */
  final List<Integer> players;

  /**
   * Creates a new GlovesGame object and initializes the list of players with integers from 1 to
   * 100.
   */
  public GlovesGame() {

    this.players = new ArrayList<>();

    for (int i = 1; i <= 50; i++) {
      this.players.add(i);
    }
  }

  @Override
  public List<Integer> getPlayers() {
    return this.players;
  }

  @Override
  public double getShapleyValue(int player) {
    return 0.5;
  }

  @Override
  public double getV(List<Integer> players) throws GameException {
    List<Integer> leftShoes = new ArrayList<>();
    List<Integer> rightShoes = new ArrayList<>();

    for (int player : players) {
      if (player <= this.players.size() / 2) {
        leftShoes.add(player);
      } else {
        rightShoes.add(player);
      }
    }

    return Math.min(leftShoes.size(), rightShoes.size());
  }
}
