package game;

/** Exception for when something goes wrong in the game. */
public class GameException extends Exception {
  /**
   * @param message The specific message why somehting went wrong.
   */
  public GameException(String message) {
    super(message);
  }
}
