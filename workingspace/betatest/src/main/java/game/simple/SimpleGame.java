package game.simple;

import game.GameException;
import game.IGame;

import java.util.ArrayList;
import java.util.List;

/** A very simple game with only 3 players for testing purposes. */
public class SimpleGame implements IGame {

  private final List<Integer> players;

  public SimpleGame() {

    this.players = new ArrayList<>();

    this.players.add(1);
    this.players.add(2);
    this.players.add(3);
  }

  @Override
  public List<Integer> getPlayers() {
    return this.players;
  }

  /**
   * These have been calculated beforehand
   *
   * @param player the player to get the shapley value from
   * @return the calculated shapley value
   */
  @Override
  public double getShapleyValue(int player) {
    switch (player) {
      case 1:
        return 29.166666666667;
      case 2:
        return 31.666666666667;
      case 3:
        return 39.166666666667;
    }
    return 0;
  }

  @Override
  public double getV(List<Integer> players) throws GameException {
    if (players.isEmpty()) return 0;

    if (players.size() == 1 && players.contains(1)) {
      return 20;
    }
    if (players.size() == 1 && players.contains(2)) {
      return 25;
    }
    if (players.size() == 1 && players.contains(3)) {
      return 30;
    }
    if (players.size() == 2 && players.contains(1) && players.contains(2)) {
      return 50;
    }
    if (players.size() == 2 && players.contains(1) && players.contains(3)) {
      return 60;
    }
    if (players.size() == 2 && players.contains(2) && players.contains(3)) {
      return 60;
    }
    if (players.size() == 3 && players.contains(1) && players.contains(2) && players.contains(3)) {
      return 100;
    }

    throw new GameException(
        "Something went wrong with the getV method! Check if the permutation exists!");
  }
}
