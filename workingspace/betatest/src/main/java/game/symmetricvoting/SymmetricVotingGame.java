package game.symmetricvoting;

import game.GameException;
import game.IGame;

import java.util.ArrayList;
import java.util.List;

/** Implementation of the symmetric voting game */
public class SymmetricVotingGame implements IGame {

  private final List<Integer> players;

  public SymmetricVotingGame() {
    this.players = new ArrayList<>();

    for (int i = 1; i <= 500; i++) {
      this.players.add(i);
    }
  }

  @Override
  public List<Integer> getPlayers() {
    return this.players;
  }

  @Override
  public double getShapleyValue(int player) {
    return 0.001;
  }

  @Override
  public double getV(List<Integer> players) throws GameException {
    return players.size() > 500 ? 1 : 0;
  }
}
