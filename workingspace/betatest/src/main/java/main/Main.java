package main;

import algorithm.AlgorithmException;
import algorithm.IAlgorithm;
import algorithm.permutationsampling.PermutationSamplingAlgorithm;
import com.google.common.collect.ListMultimap;
import game.IGame;
import game.gloves.GlovesGame;
import mse.MSECalculator;

import java.util.List;

/** The Main class, where we run the algorithm on a game */
public class Main {

  /** The number of repetitions to run the algorithm. */
  private static final int REPETITIONS = 100;
  /** The number of time steps to run the algorithm for. */
  private static final int T_STEPS = 2000;
  /** The size of points. */
  private static final int SIZE_OF_POINTS = 20;
  /** The MSE for every repetition and position. */
  private static final double[][] MSE_AT_CURRENT_REPETITION_AND_POSITION =
      new double[REPETITIONS][SIZE_OF_POINTS];
  /** The MSE mean at every position. */
  private static final double[] MSE_MEAN_AT_POSITION = new double[SIZE_OF_POINTS];
  /** The variance array. */
  private static final double[] VARIANCE_ARRAY = new double[SIZE_OF_POINTS];
  /** The standard error array. */
  private static final double[] STANDARD_ERROR_ARRAY = new double[SIZE_OF_POINTS];
  /** The current game where an approximation algorithm is to be run on */
  private static IGame currentGame;
  /** The current approximation algorithm */
  private static IAlgorithm algorithm;

  /**
   * Runs a game with an algorithm of your choice. Only run one game at a time so there are not any
   * errors.
   *
   * @param args
   * @throws AlgorithmException
   */
  public static void main(String[] args) throws AlgorithmException {

    currentGame = new GlovesGame();
    algorithm = new PermutationSamplingAlgorithm(currentGame);

    for (int i = 0; i < REPETITIONS; i++) {
      runAlgorithm(i);
    }
    calculateMSEMean();
    calculateSE();
    prettyPrint();
  }

  private static void runAlgorithm(int currentRepetition) throws AlgorithmException {
    algorithm.calculateApproximation(currentGame, T_STEPS);

    MSECalculator mseCalculator = new MSECalculator(currentGame, algorithm);

    ListMultimap<Integer, Double> multimap = algorithm.getArrayListMultimap();

    for (int i = 0; i < SIZE_OF_POINTS; i++) {
      MSE_AT_CURRENT_REPETITION_AND_POSITION[currentRepetition][i] =
          calculateMseAtCurrentPosition(currentGame, mseCalculator, multimap, i);
    }
  }

  /**
   * Calculates the mean squared error at the current position for a given game and Shapley values.
   *
   * @param game the game for which the Shapley values are calculated
   * @param mseCalculator the calculator for mean squared error
   * @param multimap the multimap containing the Shapley values for each player and position
   * @param position the current position
   * @return the mean squared error at the current position
   */
  private static double calculateMseAtCurrentPosition(
      IGame game,
      MSECalculator mseCalculator,
      ListMultimap<Integer, Double> multimap,
      int position) {
    double mseSquare = 0;
    for (int player : game.getPlayers()) {
      List<Double> shapleyValueList = multimap.get(player);
      double shapleyValue = shapleyValueList.get(position);
      mseSquare += mseCalculator.calculateMSEAtCurrentPosition(player, shapleyValue);
    }
    return mseSquare / game.getPlayers().size();
  }

  /**
   * Calculates the mean of the Mean Squared Error (MSE) for each position based on a
   * two-dimensional array of MSE values. The mean is calculated by summing up the MSE values for
   * each repetition and dividing by the total number of repetitions.
   */
  private static void calculateMSEMean() {
    double mse;
    for (int j = 0; j < SIZE_OF_POINTS; j++) {
      mse = 0;
      for (int i = 0; i < REPETITIONS; i++) {
        mse += MSE_AT_CURRENT_REPETITION_AND_POSITION[i][j];
      }
      MSE_MEAN_AT_POSITION[j] = mse / REPETITIONS;
    }
  }

  /** Calculates the Variance of all the points for the MSE */
  private static void calculateVariance() {
    double variance = 0;
    for (int j = 0; j < SIZE_OF_POINTS; j++) {
      double buffer = 0;
      for (int i = 0; i < REPETITIONS; i++) {
        buffer +=
            Math.pow(MSE_AT_CURRENT_REPETITION_AND_POSITION[i][j] - MSE_MEAN_AT_POSITION[j], 2);
      }
      variance = buffer / (REPETITIONS - 1);
      VARIANCE_ARRAY[j] = variance;
    }
  }

  /** Calculates the standard error */
  private static void calculateSE() {
    calculateVariance();
    for (int i = 0; i < SIZE_OF_POINTS; i++) {
      STANDARD_ERROR_ARRAY[i] = Math.sqrt(VARIANCE_ARRAY[i]) / Math.sqrt(REPETITIONS);
    }
  }

  /**
   * Prints the current T, MSE_MEAN and SE in a formatted and readable manner. Each value is printed
   * on a separate line with appropriate spacing. The T, MSE_MEAN, and SE values are obtained from
   * their corresponding arrays. The number of points to print is determined by the constant
   * SIZE_OF_POINTS. The T values are calculated by dividing T_STEPS by SIZE_OF_POINTS and
   * multiplying by i, where i is the index of the current point being printed.
   */
  private static void prettyPrint() {
    System.out.println();
    System.out.println();
    System.out.println("Current T            |     MSE_MEAN          |      SE");
    for (int i = 0; i < SIZE_OF_POINTS; i++) {
      System.out.print(T_STEPS / SIZE_OF_POINTS * i + "                     ");
      System.out.print(MSE_MEAN_AT_POSITION[i] + "                     ");
      System.out.print(STANDARD_ERROR_ARRAY[i]);
      System.out.println();
    }
  }
}
